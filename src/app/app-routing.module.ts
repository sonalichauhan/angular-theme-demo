import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlertComponent } from './component/alert/alert.component';
import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';

export const Approutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
        {
          path: 'component',
          component: AlertComponent
        }
    ]
  },
//   {
//     path: '**',
//     redirectTo: '/authentication/404'
//   }
];
