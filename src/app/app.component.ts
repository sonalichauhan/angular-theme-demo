import { Component, ViewEncapsulation } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'theme-angular';
  info='info';
  success='success';
  msg1='hello i am notifier of type info';
  msg2='hello i am notifier of type success';
}
