import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NotifierModule } from 'angular-notifier';
import { AppComponent } from './app.component';
import { AlertComponent } from './component/alert/alert.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ButtonComponent } from './component/button/button.component';
import { ModalComponent } from './component/modal/modal.component';
import { NgbdModalContent, NgbdModalConfirmAutofocus } from './component/modal/ngbd-modal-content.component';
import { AccordianComponent } from './component/accordian/accordian.component';
import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap';
import { DropdownComponent } from './component/dropdown/dropdown.component';
import { PaginationComponent } from './component/pagination/pagination.component';
import { ProgressbarComponent } from './component/progressbar/progressbar.component';
import { TableComponent } from './component/table/table.component';
import { HeaderComponent } from './component/header/header.component';
import { FormsModule } from '@angular/forms';
import { TimepickerComponent } from './component/timepicker/timepicker.component';
import { NotifierComponent } from './component/notifier/notifier.component';
import { BlankComponent } from './layouts/blank/blank.component';
import { FullComponent } from './layouts/full/full.component';
import { RouterModule, Routes } from '@angular/router';
import { BreadcrumbComponent } from './shared/breadcrumb/breadcrumb.component';
import { FooterComponent } from './shared/footer/footer.component';
import { SettingsComponent } from './shared/settings/settings.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { Approutes } from './app-routing.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ToasterComponent } from './component/toaster/toaster.component';
import { ToastrModule } from 'ngx-toastr';
import { TabsComponent } from './component/tabs/tabs.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 1,
  wheelPropagation: true,
  minScrollbarLength: 20
}; 

const appRoutes: Routes=[
  {
    path: '',
    component: FullComponent,
    children: [
      
        {
          path: 'progressbar',
          component: ProgressbarComponent,
          data: {
            title: 'Progressbar',
            urls: [
              { title: 'Dashboard', url: '/dashboard' },
              { title: 'Progressbar' },
              { title: 'Progressbar' }
            ]
          }
        },
        {
          path: 'pagination',
          component: PaginationComponent,
          data: {
            title: 'Pagination',
            urls: [
              { title: 'Dashboard', url: '/dashboard' },
              { title: 'Pagination' },
              { title: 'Pagination' }
            ]
          }
        },
        {
          path: 'accordion',
          component: AccordianComponent,
          data: {
            title: 'Accordion',
            urls: [
              { title: 'Dashboard', url: '/dashboard' },
              { title: 'Accordion' },
              { title: 'Accordion' }  
            ]
          }
        },
        {
          path: 'alert',
          component: AlertComponent,
          data: {
            title: 'Alert',
            urls: [
              { title: 'Dashboard', url: '/dashboard' },
              { title: 'Alert' },
              { title: 'Alert' }
            ]
          }
        },
        {
          path: 'modal',
          component: ModalComponent,
          data: {
            title: 'Modal',
            urls: [
              { title: 'Dashboard', url: '/dashboard' },
              { title: 'Modal' },
              { title: 'Modal' }
            ]
          }
        },
        {
          path: 'timepicker',
          component: TimepickerComponent,
          data: {
            title: 'Timepicker',
            urls: [
              { title: 'Dashboard', url: '/dashboard' },
              { title: 'Timepicker' },
              { title: 'Timepicker' }
            ]
          }
        },
        {
          path: 'button',
          component: ButtonComponent,
          data: {
            title: 'Button',
            urls: [
              { title: 'Dashboard', url: '/dashboard' },
              { title: 'Button' },
              { title: 'Button' }
            ]
          }
        },
        {
          path: 'notifier',
          component: NotifierComponent,
          data: {
            title: 'Notifier',
            urls: [{ title: 'Dashboard', url: '/dashboard' }, { title: 'Notifier' }, { title: 'Notifier' }]
          }
        },
        {
          path: 'toaster',
          component: ToasterComponent,
          data: {
            title: 'toaster',
            urls: [{ title: 'Dashboard', url: '/dashboard' }, { title: 'toaster' }, { title: 'toaster' }]
          }
        },
        {
          path: 'tabs',
          component: TabsComponent,
          data: {
            title: 'tabs',
            urls: [{ title: 'Dashboard', url: '/dashboard' }, { title: 'tabs' }, { title: 'tabs' }]
          }
        }
      ]
    }
];

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    ButtonComponent,
    ModalComponent,
    NgbdModalContent,
    NgbdModalConfirmAutofocus,
    AccordianComponent,
    DropdownComponent,
    PaginationComponent,
    ProgressbarComponent,
    TableComponent,
    HeaderComponent,
    TimepickerComponent,
    NotifierComponent,
    BlankComponent,
    FullComponent,
    BreadcrumbComponent,
    FooterComponent,
    SettingsComponent,
    SidebarComponent,
    ToasterComponent,
    TabsComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    NgbModule,
    FormsModule,
    NotifierModule,
    RouterModule.forRoot(appRoutes),
    PerfectScrollbarModule,
    ToastrModule.forRoot({
      timeOut : 10000,
      positionClass :  'toast-bottom-right',
      preventDuplicates : true,
      maxOpened: 5,
      autoDismiss: false,
      newestOnTop: true,
      resetTimeoutOnDuplicate: true,
      countDuplicates: true
    }),
    BrowserAnimationsModule

  ],
  entryComponents: [NgbdModalContent, NgbdModalConfirmAutofocus],
  providers: [{
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
