import { Component, OnInit, Input } from '@angular/core';
import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-accordian',
  templateUrl: './accordian.component.html',
  styleUrls: ['./accordian.component.css'],
  providers: [NgbAccordionConfig]
})
export class AccordianComponent implements OnInit {
@Input() accordian: Array<IAccordian> = [];
@Input() allowOpenAll: boolean=false;
  constructor(config: NgbAccordionConfig) {
    // customize default values of accordions used by this component tree
    config.closeOthers = !this.allowOpenAll;
    config.type = accType.light;
    this.accordian.push(
      {
        id: 'one',
        header: 'success',
        content: 'This is an success acc',
        title:'success',
        enableheader:true,
      },
      {
        id: 'two',
        header: 'info',
        content: 'This is an info acc',
        title:'info',
        enableheader:true,
      },
      {
        id: 'three',
        header: 'danger',
        content: 'This is an danger acc',
        title:'danger',
        enableheader:true,
      },
      {
        id: 'four',
        header: 'dark',
        content: 'This is an dark acc',
        title:'dark',
        enableheader:true,
      },
      {
        id: 'five',
        header: 'light',
        content: 'This is an light acc',
        title:'light',
        enableheader:true,
      },
      {
        id: 'six',
        header: 'secondary',
        content: 'This is an secondary acc',
        title:'secondary',
        enableheader:true,
      },
      {
        id: 'seven',
        header: 'primary',
        content: 'This is an primary acc',
        title:'primary',
        enableheader:true,
      },
      {
        id: 'eight',
        header: 'warning',
        content: 'This is an warning acc',
        title:'warning',
        enableheader:false,
      }
    );
  }

  ngOnInit() {
  }

}
export interface IAccordian{
  id: string,
  header: string,
  content: string,
  title: string,
  enableheader: boolean,
}
export enum accType{
  success='success',
  info='info',
  warning='warning',
  danger='danger',
  primary='primary',
  secondary='secondary',
  light='light',
  dark='dark',
}