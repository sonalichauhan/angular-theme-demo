import { Input,Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input() public alert: IAlert;
  constructor() {
    this.alert={type:Type.info,message:'hello i am test',selfClosable:false,showAlert:true};
   }

  ngOnInit() :void {
    setTimeout(() => { this.alert.showAlert=!this.alert.selfClosable;this.alert.selfClosable = false;}, 5000);
  }
  public closeAlert(alert: IAlert) {
    this.alert.showAlert=false;
  }
}
export interface IAlert{
  type: Type;
  message: string;
  selfClosable: boolean;
  showAlert: boolean;
}
enum Type {
  success='success',
  info='info',
  warning='warning',
  danger='danger'
}