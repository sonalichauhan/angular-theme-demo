import { Component, OnInit, Input } from '@angular/core';
import { Size } from '../modal/ngbd-modal-content.component'
@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
@Input() button: IButton= {type: BType.info, text: 'I am info button',isrounded:false,
isoutlined:false,size:Size.sm,isblock:false,extraclass:''};
  constructor() { }

  ngOnInit() {
  }

}
export interface IButton{
  type: BType,
  text: string,
  isrounded: boolean,
  isoutlined: boolean,
  size: Size,
  isblock: boolean,
  extraclass: string
}
export enum BType{
  primary='primary',
  secondary='secondary',
  success='success',
  info='info',
  warning='warning',
  danger='danger'
}