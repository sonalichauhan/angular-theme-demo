import { Input, Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent, IModal, Size, ModalType, MODALS, NgbdModalConfirmAutofocus } from './ngbd-modal-content.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-modal',
  template: '',
  styleUrls: ['./modal.component.css']
})



export class ModalComponent implements OnInit {
@Input() public modal: IModal;
  constructor(private modalService: NgbModal) { 
    this.modal={
      title: 'Test Modal',
      body: 'Hello I am a test modal',
      style: {
        centered: true,
        size: Size.sm,
        scrollable: true
      },
      type: ModalType.Confirm,
      backdropClass:'light-blue-backdrop',
      windowClass:'dark-modal',
      hideCloseButton: true,
      closeOnOutsideClick: false,
      animation: true,
      keyboard: false
    };
    this.open();
    
  }
  open() {
    const modalRef = this.modalService.open(MODALS[this.modal.type], this.modal.style);
    modalRef.componentInstance.modal = this.modal;
  }
  ngOnInit() {
  }

}

