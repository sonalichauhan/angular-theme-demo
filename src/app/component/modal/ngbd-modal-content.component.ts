import { NgModule, Input, Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
    selector: 'app-ngbd-modal-content',
    templateUrl: './ngbd-modal-content.component.html',
  })
  export class NgbdModalContent {
    @Input() modal: IModal;
  
    constructor(public activeModal: NgbActiveModal) {
//         this.modal={
//             title: 'PROFILE',
//             body: 'ARE yOU SURE WITH THE PROFILE',
//             style: {
//               centered: true,
//               size: Size.lg,
//               scrollable: true
//             },
//             type: ModalType.Confirm,
//             backdropClass:'light-blue-backdrop',
//   windowClass:'dark-modal',
//   hideCloseButton: true,
//   closeOnOutsideClick: false,
//   animation: true,
//   keyboard: false
//           };
    }
  }
  
  @Component({
    selector: 'ngbd-modal-confirm-autofocus',
    template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-title">{{modal.title}}</h4>
      <button type="button" class="close" aria-label="Close button" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    {{modal.body}}
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Cancel</button>
      <button type="button" ngbAutofocus class="btn btn-danger" (click)="activeModal.close('Ok click')">Ok</button>
    </div>
    `
  })
  export class NgbdModalConfirmAutofocus {
    @Input() modal: IModal;
        constructor(public activeModal: NgbActiveModal) {
//             this.modal={
//                 title: 'jshfj',
//                 body: 'sjdhjhd',
//                 style: {
//                   centered: true,
//                   size: Size.lg,
//                   scrollable: true
//                 },
//                 type: ModalType.Basic,
//                 backdropClass:'light-blue-backdrop',
//   windowClass:'dark-modal',
//   hideCloseButton: true,
//   closeOnOutsideClick: false,
//   animation: true,
//   keyboard: false
//               };
        }
  }
  
  export const MODALS = {
    Basic: NgbdModalContent,
    Confirm: NgbdModalConfirmAutofocus
  };
  
 
  export interface IModal
{
  title: string,
  body: string,
  style: {
    centered: boolean,
    size: Size,
    scrollable: boolean
  },
  type: ModalType,
  backdropClass:'light-blue-backdrop',
  windowClass:'dark-modal',
  hideCloseButton: true,
  closeOnOutsideClick: false,
  animation: true,
  keyboard: false

}
export enum Size{
  lg = 'lg',
  sm = 'sm',
  xl = 'xl'
}
export enum ModalType {
    Basic= 'Basic',
    Confirm= 'Confirm'
}