import { Input, Component, OnInit } from '@angular/core';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
  providers: [NgbPaginationConfig]
})
export class PaginationComponent implements OnInit {
  @Input() page:number=1;
  @Input() collectionSize:number=1005;
  @Input() pageSize:number=100;
  constructor(config: NgbPaginationConfig) {
    // config.size = '-';
    config.boundaryLinks=true;
    config.rotate=true;
    config.maxSize=7;
    config.ellipses=true;
   }

  ngOnInit() {
  }

}
