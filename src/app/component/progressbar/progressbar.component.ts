import { Component, OnInit, Input } from '@angular/core';
import { config } from 'rxjs';
import { NgbProgressbarConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.css'],
  providers: [NgbProgressbarConfig]
})
export class ProgressbarComponent implements OnInit {
@Input() value:number=27;
@Input() type:PBType=PBType.info;
@Input() progressText:string='progressing';
@Input() max:number=50;
  constructor(config: NgbProgressbarConfig) { 
    config.striped=true;
    config.animated=true;
    config.showValue=false;
    config.height="20px";
  }

  ngOnInit() {
  }

}
export enum PBType
{
  success='success',
  info='info',
  warning='warning',
  danger='danger'
}