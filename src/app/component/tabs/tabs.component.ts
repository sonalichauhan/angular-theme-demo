import { Component, OnInit, Input } from '@angular/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent {
  @Input() tabJustify: ITabJustify = ITabJustify.justified;
  @Input() tabtype: ITabType=ITabType.pills;
  @Input() contenttabs: ITab[]=[{title:'one tab',content:`Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table
  craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl
  cillum PBR. Homo nostrud organic, assumendaRaw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth
  master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh
  dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum
  iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.`,disabled:false},
  {title:'two tab',content:`Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth
  master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh
  dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum
  iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.`,disabled:false},
  {title:'disabled tab',content:'three content',disabled:true}];
  currentOrientation = 'horizontal';
  public beforeChange($event: NgbTabChangeEvent) {
    if ($event.nextId === 'tab-preventchange2') {
      $event.preventDefault();
    }
  }
}
enum ITabJustify{
  start="start",
  center="center",
  end="end",
  fill="fill",
  justified="justified"
}
enum ITabType{
  tabs="tabs",
  pills="pills",
}
export interface ITab{
  title:string,
  content: string,
  disabled:boolean,
}