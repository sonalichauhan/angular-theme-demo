import { Component, OnInit, Injectable, Output, Input } from '@angular/core';
import { NgbTimeStruct, NgbTimeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';

// @Injectable()
// export class NgbTimeStringAdapter extends NgbTimeAdapter<string> {
  

//   fromModel(value: string): NgbTimeStruct {
//     if (!value) {
//       return null;
//     }
//     const split = value.split(':');
//     return {
//       hour: parseInt(split[0], 10),
//       minute: parseInt(split[1], 10),
//       second: parseInt(split[2], 10)
//     };
//   }

//   toModel(time1: NgbTimeStruct): string {
//     if (!time1) {
//       return null;
//     }
//     return `${this.pad(time1.hour)}:${this.pad(time1.minute)}:${this.pad(time1.second)}`;
//   }

//   private pad(i: number): string {
//     return i < 10 ? `0${i}` : `${i}`;
//   }
// }

@Component({
  selector: 'app-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.css'],
})
export class TimepickerComponent implements OnInit {
  @Input() timepicker: ITime = {title: 'test',
    subtitle: 'time test',
    meridian: false,
    seconds: true,
    spinners: true};
  // @Output() time;
  time = { hour: 13, minute: 30 , second: 0 };




  ctrl = new FormControl('', (control: FormControl) => {
    const value = control.value;

    if (!value) {
      return null;
    }

    if (value.hour < 12) {
      return { tooEarly: true };
    }
    if (value.hour > 13) {
      return { tooLate: true };
    }

    return null;
  });

  // toggleMeridian() {
  //   this.timepicker.meridian = !this.timepicker.meridian;
  // }

  // toggleSeconds() {
  //   this.timepicker.seconds = !this.timepicker.seconds;
  // }

  // toggleSpinners() {
  //   this.timepicker.spinners = !this.timepicker.spinners;
  // }
  constructor() { }

  ngOnInit() {
  }

}
export interface ITime
{
  title: string,
  subtitle: string,
  meridian: boolean,
  seconds: boolean,
  spinners: boolean
}