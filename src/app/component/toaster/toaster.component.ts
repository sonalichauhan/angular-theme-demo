import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
  styles: [`#toast-container > div {
    opacity:1;
  }`]
})
export class ToasterComponent  {
  @Input() message: string='Hello I am test toaster';
  @Input() title: string='info';
  constructor(private toastrs: ToastrService) {
    this.showInfo();
    
  }

  showSuccess() {
    this.toastrs.success(this.message, this.title);
  }

  showError() {
    this.toastrs.error(this.message, this.title);
  }

  showWarning() {
    this.toastrs.warning(this.message, this.title);
  }

  showInfo() {
    this.toastrs.info(this.message, this.title);
  }

}
export interface IToast  {
  timeOut : 10000,
  positionClass :  'toast-bottom-right',
  preventDuplicates : true,
  maxOpened: 5,
  autoDismiss: false,
  newestOnTop: true,
  resetTimeoutOnDuplicate: true,
  countDuplicates: true
}